#include <stdio.h>
#include <math.h>
#include <stdbool.h>

// Написать программу, которая принимает на вход массу, плотность, и высоту цилиндра и вычисляет его диаметр.
// Важно оформить «красивый» ввод/вывод и выводить ошибку, если введённые параметры не корректные
// по физическому смыслу.


bool InputMass(int* m) {
    printf("Enter mass of cylinder: ");
    scanf("%d", m);
    return *m > 0;
}

bool InputDensity(int *ro) {
    printf("Enter density of cylinder: ");
    scanf("%d", ro);
    return *ro > 0;
}

bool InputHeight(int* h) {
    printf("Enter height of cylinder: ");
    scanf("%d", h);
    return *h > 0;
}

double CountDiameter(int m, int ro, int h) {
    return sqrt(4 * m / (M_PI * h * ro));
}

int main() {
    int m;
    int ro;
    int h;

    printf("COUNT CYLINDER DIAMETER\n");
    printf("-----------------------\n");

    while (!InputMass(&m)) {
        printf("Error mass value! \n");
    }

    while (!InputDensity(&ro)) {
        printf("Error density value! \n");
    }

    while (!InputHeight(&h)) {
        printf("Error height value! \n");
    }

    printf("-----------------------\n");
    printf(" Cylinder diameter: %lf", CountDiameter(m,ro,h));

    return 0;
}
